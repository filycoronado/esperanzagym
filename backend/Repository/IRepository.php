<?
namespace app\Repository;
use app\models\Account;
interface IRepository{
  public function  Save(Account $obj);
  public function Update();
  public  function Delete();
  public function  List();
}