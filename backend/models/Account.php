<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "account".
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $create_date
 * @property int|null $enable
 */
class Account extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'account';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'email', 'password', 'create_date'], 'required'],
            [['id', 'enable'], 'integer'],
            [['create_date'], 'safe'],
            [['email', 'password'], 'string', 'max' => 45],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'password' => 'Password',
            'create_date' => 'Create Date',
            'enable' => 'Enable',
        ];
    }
}
